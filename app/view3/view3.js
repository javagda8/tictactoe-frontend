'use strict';

angular.module('myApp.view3', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/view3', {
            templateUrl: 'view3/view3.html',
            controller: 'View3Ctrl'
        });
    }])

    .controller('View3Ctrl', ['$http', '$routeParams', '$window', 'AuthService', function ($http, $routeParams, $window, AuthService) {
        var URL = "http://localhost:8080";
        var self = this;
        this.connectedToRoom = $routeParams.roomId;
        this.connectedUsers = [];

        this.registerFrame = {
            'roomId': this.connectedToRoom,
            'userId': AuthService.loggedInUser
        };

        console.log('You (' + (AuthService.loggedInUser) + ') are now connected to room: ' + this.connectedToRoom);

        this.checkRoom = function () {
            $http.get(URL + "/room/get/" + self.connectedToRoom)
                .then(function (odpowiedz) {
                        console.log(odpowiedz);
                        // zarejestruj się do pokoju
                        self.registerToRoom();
                    },
                    function (odpowiedzNaBlad) {
                        $window.location = "#!/view1";
                    });
        };

        this.registerToRoom = function () {
            $http.post(URL + '/room/connect', self.registerFrame)
                .then(function (odpowiedz) {
                    console.log(odpowiedz);
                    self.connectedUsers = [];
                    for (var index in odpowiedz.data.connectedUsers) {
                        self.connectedUsers.push(odpowiedz.data.connectedUsers[index]);
                    }
                }, function (odpowiedzNaBlad) {
                    console.log(odpowiedzNaBlad);
                });
        };
        this.checkRoom();
    }]);