'use strict';

angular.module('myApp.view4', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/view4', {
            templateUrl: 'view4/view4.html',
            controller: 'View4Ctrl'
        });
    }])

    .controller('View4Ctrl', ['$http', '$routeParams', '$window', 'AuthService', function ($http, $routeParams, $window, AuthService) {
        var URL = "http://localhost:8080";
        var self = this;
        this.changeUser = $routeParams.changeUser;
        AuthService.loggedInUser = this.changeUser;

        console.log(AuthService.loggedInUser);
        $window.location = "#!/view1";
    }]);