'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
    'ngRoute',
    'myApp.view1',
    'myApp.view2',
    'myApp.view3',
    'myApp.view4',
    'myApp.viewLogin',
    'myApp.viewRegister',
    'myApp.roomService',
    'myApp.authService',
    'ui.router',
    'myApp.version'
]).config(['$locationProvider', '$routeProvider', '$stateProvider', function ($locationProvider, $routeProvider, $stateProvider) {
    $locationProvider.hashPrefix('!');

    $routeProvider.otherwise({redirectTo: '/view1'});

    $stateProvider
        .state('home', {
            url: '/view1',
            templateUrl: 'view1/index.html',
            controller: 'mainCtrl'
        })
        .state('boards', {
            url: '/boards',
            templateUrl: 'boards/board.html',
            controller: 'boardCtrl'
        });

}]).run(['$http', '$window', 'AuthService', '$rootScope', function ($http, $window, AuthService, $rootScope) {
    //
    // $rootScope.$on('$stateChangeStart', function (event, toState, fromState) {
    //     console.log("change" + toState);
    // });

    if (AuthService.loggedInUser === '') {
        console.log('redirect');
        // jesteśmy niezalogowani
        var token = $window.sessionStorage.getItem('token');
        var user_id = $window.sessionStorage.getItem('user_id');

        if (token == null || token === undefined || user_id == null || user_id === undefined) {
            // to znaczy ze nie mamy danych logowania w sesji
            $window.location = "#!/viewLogin";
        } else {
            $http.defaults.headers.common['Authorization'] = 'Bearer ' + token;
            AuthService.loggedInUser = user_id;
        }
    }
}]);
